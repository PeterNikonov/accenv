<?php

namespace Accenv;

    use \My\MyPdo;
    use \MyPractic\MyQuery;

    use Accenv\Config;

class Data {

    /**
     * Создать ресурсы бд
     */
    public function Init() {
        // таблица учета лицензий
        $q = "CREATE TABLE IF NOT EXISTS `".  Config::ACCESS_TABLE ."` (
`id` int(14) NOT NULL AUTO_INCREMENT,
`user` int(14) NOT NULL,
`pg` TEXT NULL,
`dg` TEXT NULL,
`rg` TEXT NULL,
`block` int NOT NULL,

    UNIQUE (`user`),

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='';";

        MyPdo::get()->query($q);
    }
    /**
     * Получить разрешения для указанного пользователя
     */
    public function getAccess($user) {
           $stmt = MyPdo::get()->query("SELECT * FROM ". Config::ACCESS_TABLE ." WHERE user = ". (int) $user."");
        if($stmt) {
               $row = $stmt -> fetch(PDO::FETCH_ASSOC);
               return $row;
        }
    }
    /**
     * создать ресурс полльзователя
     */
    public function setAccess($user) {
        MyPdo::get()->query((new MyQuery(Config::ACCESS_TABLE, ['user' => (int) $user, 'block' => 0]))-> Insert());
    }
    /**
     * установить роль для указанного пользователя
     */
    public function setRg($user, $rg) {
        MyPdo::get()-> query("UPDATE ". Config::ACCESS_TABLE. " SET rg = ". MyPdo::get()->quote($rg)." WHERE user = ". (int) $user."");
    }
    /**
     * установить сетку разрешений для указанного пользователя
     */
    public function setPg($user, $pg) {
        MyPdo::get()-> query("UPDATE ". Config::ACCESS_TABLE. " SET pg = ". MyPdo::get()->quote($pg)." WHERE user = ". (int) $user."");
    }
    /**
     * установить подразделения для указанного пользователя
     */
    public function setDg($user, $dg) {
        MyPdo::get()-> query("UPDATE ". Config::ACCESS_TABLE. " SET dg = ". MyPdo::get()->quote($dg)." WHERE user = ". (int) $user."");
    }
    /**
     * блокировать пользователя
     */
    public function setBlock($user, $block = 1) {
        MyPdo::get()-> query("UPDATE ". Config::ACCESS_TABLE. " SET block = ". (int) $block ." WHERE user = ". (int) $user."");
    }
}
