<?php

namespace Accenv;

    use \Accenv\Data;
    use \Accenv\Config;

    use \MyPractic\Stringf;
    use \MyPractic\Filef;
    
    use \Usenv\Data as UserData;

class Action {

    /**
     * Выводит переданный список пользователей
     */
    public function userList($list) {

        $d = new Data;

        if($list) {
            $path = '';
            foreach ($list as $u) {
                $perm = $d->getAccess($u['id']);
                $u['block'] = ($perm['block']===1) ? 'Заблокирован' : 'Активен';
                $path.='';
                        Stringf::k2v(Filef::Read(Config::GetDir().'/html/userList.html'), $u);
            }
            return $path;
        } else { throw new \Exception('has no user list'); }
    }

    /**
     * Выводит сетку для переданного пользователя
     */
    public function userDetail($userId) {

        if($userId) {

                    $d = new Data;
            $perm = $d->getAccess($userId);

            $ud = new UserData();
            $ud -> checkUser($email = false, $password = false, $userId);
            
            $blockBtn = $this->drawBlockButton($perm['block']);
            $permissionTable = $this->drawPermissionTable($perm['pg']);

            // 

        }
    }
    
    /**
     * Таблица с инпутами для расстановки прав
     * @param type $userPg
     * @return string
     */
    protected function drawPermissionTable($userPg) {
           
        $c = new Config;

           $access_grid = $c->access_grid;
        if($access_grid) {
            foreach($access_grid as $array) {

                $ret.='<p><strong>'.$array['name'].'</strong></p>'; $features = '';
                if($array['features']) {
                    $features.= '<table class="table table-condensed table-hover">';
                    foreach($array['features'] as $feat => $feat_name) {
                        $checked_0 = ''; $checked_1 = '';

                        if($userPg[$array['mode']][$feat] === 0) { $checked_0 = "checked"; }
                        if($userPg[$array['mode']][$feat] === 1) { $checked_1 = "checked"; }

                    $features.= '<tr><td width="70%">'.$feat_name.'</td>'
                              . '<td>'
                              . '<label class="radio-inline">
                <input type="radio" name="permission['.$array['mode'].']['.$feat.']" value="0" '.$checked_0.'> Нет
                </label>
                <label class="radio-inline">
                <input type="radio" name="permission['.$array['mode'].']['.$feat.']" value="1" '.$checked_1.'> Да
                </label>'
                            . '</td>'
                            . '</tr>';
                    } $features.= '</table>';
                }  $ret.=$features;
            }
            return $ret;
        }
    }

    /**
     * Кнопка управления доступом
     */
    protected function drawBlockButton($block) {
        if($block === 1) { $btn = '<a href="%href%&set=0" class="btn btn-danger btn-xs">Запретить</a>'; }
        if($block === 0) { $btn = '<a href="%href%&set=1" class="btn btn-success btn-xs">Разрешить</a>'; }
        return $btn;
    }
}
