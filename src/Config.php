<?php

    namespace Accenv;

class Config {
    const ACCESS_TABLE = 'env_access';
    /** 
     * Массив задает варианты доступа
     * @var array
     */
    public $access_grid = [
        [
        'mode' => 'client', 
        'name' => 'Клиенты', 
        'features' => [
            'search' => 'Поиск',
            'create' => 'Создание',
            'edit' => 'Редактирование',
            'delete' => 'Удаление',
            ]
        ]
    ];

    public static function GetDir() { return __DIR__; }

}